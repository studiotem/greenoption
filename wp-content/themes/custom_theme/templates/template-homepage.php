<?php
/**
 * Template Name: Homepage
 *
 * @package WordPress
 * @subpackage custom_theme
 * @author Studio TEM
 */

get_header();

include_once get_stylesheet_directory() . '/templates/partials/hero.php';

get_footer();
